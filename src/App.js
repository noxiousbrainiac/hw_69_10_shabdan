import React from 'react';
import FilmFinder from "./containers/FilmFinder/FilmFinder";
import {Switch} from "react-router-dom";
import {Route} from "react-router-dom";
import ShowResult from "./components/ShowResult/ShowResult";

const App = () => {
    return (
        <>
            <FilmFinder/>
            <Switch>
                <Route path="/shows/:id" component={ShowResult}/>
            </Switch>
        </>
    );
};

export default App;