export const FETCH_DATA = "FETCH_DATA";
export const SHOW_RESULT = "SHOW_RESULT";
export const INPUT_CHANGE = "INPUT_CHANGE";

export const showResult = payload => ({type: SHOW_RESULT, payload});
export const fetchData = payload => ({type:FETCH_DATA,  payload});
export const inputChange = payload => ({type: INPUT_CHANGE, payload});