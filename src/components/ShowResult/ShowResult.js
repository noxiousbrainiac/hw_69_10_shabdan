import React, {useEffect, useReducer} from 'react';
import {Container} from "@material-ui/core";
import {SHOW_RESULT, showResult} from "../../store/actions/actions";
import {useParams} from "react-router-dom";
import axiosApi from "../../axiosApi";

const initialState = {
    film: {}
}

const reducer = (state, action) => {
    switch (action.type) {
        case SHOW_RESULT:
            return {
                ...state,
                film: action.payload
            }
        default:
            return state;
    }
}

const ShowResult = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const {film} = state;
    const {id} = useParams();

    const showFilm = async () => {
        const {data} = await axiosApi.get(`/shows/${id}`);
        dispatch(showResult(data));
    }

    useEffect(() => {
        showFilm();
    }, [id])

    const removeHTML = (str) =>{
        let tmp = document.createElement("DIV");
        tmp.innerHTML = str;
        return tmp.textContent || tmp.innerText || "";
    }

    return (
        <Container>
            <div style={{marginTop: "40px", display: "flex"}}>
                <div style={{width: "200px"}}>
                    <img src={film?.image?.medium} alt="" style={{width: "100%"}}/>
                </div>
                <div style={{width: "400px", marginLeft: "50px"}}>
                    <h2>{film?.name}</h2>
                    {removeHTML(film?.summary)}
                </div>
            </div>
        </Container>
    );
};

export default ShowResult;