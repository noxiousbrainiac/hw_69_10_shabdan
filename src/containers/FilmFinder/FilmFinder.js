import React, {useReducer} from 'react';
import axiosApi from "../../axiosApi";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {Button, Container} from "@material-ui/core";
import {FETCH_DATA, fetchData, INPUT_CHANGE, inputChange} from "../../store/actions/actions";
import {useHistory} from "react-router-dom";

const initialState = {
    films: [],
    result: {},
    input: ""
}

const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_DATA:
            return {
                ...state,
                films: action.payload
            }
        case INPUT_CHANGE:
            return {
                ...state,
                input: action.payload
            }
        default:
            return state;
    }
}

const FilmFinder = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const {films, input} = state;
    const history = useHistory();

    const getFilms = async (name) => {
        const {data} = await axiosApi.get(`/search/shows?q=${name}`);
        const newData = data.map(film => {
            return {
                title: film.show.name,
                image: film.show.image,
                id: film.show.id
            }
        })
        dispatch(fetchData(newData));
    }

    const onInputChange = (title) => {
        dispatch(inputChange(title))
    }

    const showFilm = () => {
        if (input.id) {
            history.push(`/shows/${input.id}`);
        }
    }

    return (
        <Container style={{display: "flex", alignItems: "center"}}>
            <Autocomplete
                id="combo-box-demo"
                options={films}
                onChange={(e, value) => onInputChange(value)}
                getOptionLabel={(option) => option.title}
                style={{ width: 300 }}
                renderInput={(params) => <TextField {...params} onChange={e => getFilms(e.target.value)} label="Search" variant="outlined" />}
            />
            <Button
                style={{marginLeft: "10px"}}
                variant="contained"
                color="secondary"
                onClick={showFilm}
            >
                Show
            </Button>
        </Container>
    );
};

export default FilmFinder;